using System.Data;
using System.Threading.Tasks;
using LinqToDB.Data;

namespace HubPeople.Data
{
    public interface ITransactionManager
    {
        Task BeginTransactionAsync(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted);

        Task CommitAsync();

        Task RollbackAsync();
    }

    internal class TransactionManager : ITransactionManager
    {
        private readonly DataConnection _dataConnection;

        public TransactionManager(DataConnection dataConnection)
        {
            _dataConnection = dataConnection;
        }

        public Task BeginTransactionAsync(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            return _dataConnection.BeginTransactionAsync(isolationLevel);
        }

        public Task CommitAsync()
        {
            return _dataConnection.CommitTransactionAsync();
        }

        public Task RollbackAsync()
        {
            return _dataConnection.RollbackTransactionAsync();
        }
    }
}
