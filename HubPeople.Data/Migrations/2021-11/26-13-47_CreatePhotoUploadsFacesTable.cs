using System.Data;
using FluentMigrator;

namespace HubPeople.Data.Migrations
{
    [HubPeopleMigration(2021, 11, 26, 13, 47)]
    public class CreatePhotoUploadsFacesTable : AutoReversingMigration
    {
        public override void Up()
        {
            Create.Table("PhotoUploadsFaces")
                .WithColumn("Id").AsInt32().PrimaryKey().Identity()
                .WithColumn("PhotoUploadId").AsInt32().NotNullable()
                .ForeignKey("PhotoUploads", "Id").OnDeleteOrUpdate(Rule.Cascade)
                .WithColumn("PixlabId").AsInt32().NotNullable()
                .WithColumn("PositionLeft").AsInt32().NotNullable()
                .WithColumn("PositionTop").AsInt32().NotNullable()
                .WithColumn("Width").AsInt32().NotNullable()
                .WithColumn("Height").AsInt32().NotNullable();
        }
    }
}
