using FluentMigrator;

namespace HubPeople.Data.Migrations
{
    [HubPeopleMigration(2021, 11, 26, 13, 43)]
    public class CreatePhotoUploadsTable : AutoReversingMigration
    {
        public override void Up()
        {
            Create.Table("PhotoUploads")
                .WithColumn("Id").AsInt32().PrimaryKey().Identity()
                .WithColumn("PhotoUrl").AsString(4000).NotNullable()
                .WithColumn("FoundFacesCount").AsInt32().NotNullable();
        }
    }
}
