using FluentMigrator;

namespace HubPeople.Data.Migrations
{
    public class HubPeopleMigrationAttribute : MigrationAttribute
    {
        public HubPeopleMigrationAttribute(
            int year,
            int month,
            int day,
            int hour,
            int minute,
            TransactionBehavior transactionBehavior = TransactionBehavior.Default,
            string description = null)
            : base(CalculateValue(year, month, day, hour, minute), transactionBehavior, description)
        {
        }

        private static long CalculateValue(int year, int month, int day, int hour, int minute)
        {
            return (100000000L * year) + (1000000L * month) + (10000L * day) + (100L * hour) + minute;
        }
    }
}
