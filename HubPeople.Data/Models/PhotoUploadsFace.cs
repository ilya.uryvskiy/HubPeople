using LinqToDB.Mapping;

namespace HubPeople.Data.Models
{
    [Table("PhotoUploadsFaces")]
    public class PhotoUploadsFace : BaseEntity
    {
        [Column("PhotoUploadId")]
        public int PhotoUploadId { get; set; }

        [Column("PixlabId")]
        public int PixlabId { get; set; }

        [Column("PositionLeft")]
        public int PositionLeft { get; set; }

        [Column("PositionTop")]
        public int PositionTop { get; set; }

        [Column("Width")]
        public int Width { get; set; }

        [Column("Height")]
        public int Height { get; set; }
    }
}
