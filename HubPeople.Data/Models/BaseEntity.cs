using LinqToDB.Mapping;

namespace HubPeople.Data.Models
{
    public class BaseEntity
    {
        protected BaseEntity()
        {

        }

        [PrimaryKey, Identity]
        public int Id { get; set; }
    }
}
