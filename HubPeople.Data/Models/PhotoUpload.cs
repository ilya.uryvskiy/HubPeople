using LinqToDB.Mapping;

namespace HubPeople.Data.Models
{
    [Table("PhotoUploads")]
    public class PhotoUpload : BaseEntity
    {
        [Column("PhotoUrl")]
        public string PhotoUrl { get; set; }

        [Column("FoundFacesCount")]
        public int FoundFacesCount { get; set; }
    }
}
