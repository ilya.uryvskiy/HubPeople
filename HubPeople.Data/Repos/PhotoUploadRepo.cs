using HubPeople.Data.Models;
using LinqToDB.Data;

namespace HubPeople.Data.Repos
{
    public interface IPhotoUploadRepo : IBaseRepo<PhotoUpload>
    {
    }

    internal class PhotoUploadRepo : BaseRepo<PhotoUpload>, IPhotoUploadRepo
    {
        public PhotoUploadRepo(DataConnection dataConnection) : base(dataConnection)
        {
        }
    }
}
