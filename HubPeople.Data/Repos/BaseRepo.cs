using System.Threading.Tasks;
using HubPeople.Data.Models;
using LinqToDB;
using LinqToDB.Data;

namespace HubPeople.Data.Repos
{
    public interface IBaseRepo<TEntity>
        where TEntity : BaseEntity, new()
    {
        Task<TEntity> InsertAsync(TEntity entity);
    }


    internal class BaseRepo<TEntity> : IBaseRepo<TEntity>
        where TEntity : BaseEntity, new()
    {
        protected BaseRepo(DataConnection dataConnection)
        {
            DataConnection = dataConnection;
        }

        protected DataConnection DataConnection { get; private set; }

        public async Task<TEntity> InsertAsync(TEntity entity)
        {
            entity.Id = await this.DataConnection.InsertWithInt32IdentityAsync(entity);
            return entity;
        }
    }
}
