using HubPeople.Data.Models;
using LinqToDB.Data;

namespace HubPeople.Data.Repos
{
    public interface IPhotoUploadsFaceRepo : IBaseRepo<PhotoUploadsFace>
    {}

    internal class PhotoUploadsFaceRepo : BaseRepo<PhotoUploadsFace>, IPhotoUploadsFaceRepo
    {
        public PhotoUploadsFaceRepo(DataConnection dataConnection) : base(dataConnection)
        {
        }
    }
}
