using System.Collections.Generic;
using System.Linq;
using LinqToDB.Configuration;

namespace HubPeople.Data
{
    internal class DbSettings : ILinqToDBSettings
    {
        private readonly string _connectionString;

        public DbSettings(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IEnumerable<IDataProviderSettings> DataProviders => Enumerable.Empty<IDataProviderSettings>();

        public string DefaultConfiguration => "SqlServer";

        public string DefaultDataProvider => "SqlServer";

        public IEnumerable<IConnectionStringSettings> ConnectionStrings
        {
            get
            {
                yield return
                    new ConnectionStringSettings
                    {
                        Name = "HubPeople",
                        ProviderName = "SqlServer",
                        ConnectionString = _connectionString
                    };
            }
        }
    }

    internal class ConnectionStringSettings : IConnectionStringSettings
    {
        public string ConnectionString { get; set; }

        public string Name { get; set; }

        public string ProviderName { get; set; }

        public bool IsGlobal => false;
    }
}
