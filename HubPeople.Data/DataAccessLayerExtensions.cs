﻿using System;
using System.Reflection;
using FluentMigrator.Runner;
using HubPeople.Data.Repos;
using LinqToDB.Data;
using Microsoft.Extensions.DependencyInjection;

namespace HubPeople.Data
{
    public static class DataAccessLayerExtensions
    {
        public static IServiceCollection AddDataAccessLayer(
            this IServiceCollection services,
            Action<DataAccessLayerOptions> configure)
        {
            var options = new DataAccessLayerOptions();
            configure(options);

            services.AddFluentMigratorCore()
                .ConfigureRunner(rb => rb
                    .AddSqlServer()
                    .WithGlobalConnectionString(options.ConnectionString)
                    .ScanIn(Assembly.GetExecutingAssembly()).For.Migrations());

            DataConnection.DefaultSettings = new DbSettings(options.ConnectionString);
            services.AddScoped<DataConnection>();
            services.AddTransient<IPhotoUploadRepo, PhotoUploadRepo>();
            services.AddTransient<IPhotoUploadsFaceRepo, PhotoUploadsFaceRepo>();

            services.AddTransient<ITransactionManager, TransactionManager>();

            return services;
        }
    }
}
