using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HubPeople.ApplicationServices;
using HubPeople.Data;
using HubPeople.Pixlab;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace HubPeople
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            services.Configure<PixlabConfiguration>(Configuration.GetSection("Pixlab"));
            services.AddHttpClient<IPixlabClient, PixlabClient>();

            services.AddTransient<FindFacesService>();

            services.AddDataAccessLayer(opt =>
            {
                opt.ConnectionString = Configuration.GetConnectionString("HubPeople");
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
