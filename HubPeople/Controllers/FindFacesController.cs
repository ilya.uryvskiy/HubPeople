using System;
using System.Threading.Tasks;
using HubPeople.ApplicationServices;
using HubPeople.ViewModels.FindFaces;
using Microsoft.AspNetCore.Mvc;

namespace HubPeople.Controllers
{
    [Route("")]
    public class FindFacesController : Controller
    {
        private readonly FindFacesService _findFacesService;

        public FindFacesController(FindFacesService findFacesService)
        {
            _findFacesService = findFacesService ?? throw new ArgumentNullException(nameof(findFacesService));
        }

        [HttpGet("")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost("find-faces")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> FindFaces([FromForm]string photoUrl)
        {
            var result = await _findFacesService.ProcessFaceFindingAsync(photoUrl);
            var viewModel = new FindFacesViewModel
            {
                IsSuccess = result.IsSuccess,
                ErrorMessage = result.IsSuccess ? null : result.Error,
                PhotoUrl = result.IsSuccess ? result.Value : null
            };
            return View(viewModel);
        }
    }
}
