using System;
using System.Linq;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using HubPeople.Data;
using HubPeople.Data.Models;
using HubPeople.Data.Repos;
using HubPeople.Pixlab;
using HubPeople.Pixlab.Models;
using Microsoft.Extensions.Logging;

namespace HubPeople.ApplicationServices
{
    public class FindFacesService
    {
        private readonly ITransactionManager _transactionManager;
        private readonly IPhotoUploadRepo _photoUploadRepo;
        private readonly IPhotoUploadsFaceRepo _photoUploadsFaceRepo;
        private readonly IPixlabClient _pixlabClient;
        private readonly ILogger<FindFacesService> _logger;

        public FindFacesService(
            ITransactionManager transactionManager,
            IPhotoUploadRepo photoUploadRepo,
            IPhotoUploadsFaceRepo photoUploadsFaceRepo,
            IPixlabClient pixlabClient,
            ILogger<FindFacesService> logger)
        {
            _transactionManager = transactionManager ?? throw new ArgumentNullException(nameof(transactionManager));
            _photoUploadRepo = photoUploadRepo ?? throw new ArgumentNullException(nameof(photoUploadRepo));
            _photoUploadsFaceRepo = photoUploadsFaceRepo ?? throw new ArgumentNullException(nameof(photoUploadsFaceRepo));
            _pixlabClient = pixlabClient ?? throw new ArgumentNullException(nameof(pixlabClient));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Result<string>> ProcessFaceFindingAsync(string originalPhotoUrl)
        {
            var faceDetectResponse = await _pixlabClient.DetectFacesAsync(originalPhotoUrl);

            if (faceDetectResponse.IsFailure)
            {
                _logger.LogWarning(
                    "Couldn't detect faces: {Status} {Error} {Image}",
                    faceDetectResponse.Status,
                    faceDetectResponse.Error,
                    originalPhotoUrl);
                return Result.Failure<string>(faceDetectResponse.Error);
            }

            var saveResult = await SaveFaceDetectResponseAsync(originalPhotoUrl, faceDetectResponse);
            if (saveResult.IsFailure)
            {
                return Result.Failure<string>(saveResult.Error);
            }

            if (faceDetectResponse.Faces.Length == 0)
            {
                return Result.Failure<string>("No faces found on your image!");
            }

            var faceRectangles = faceDetectResponse.Faces.Select(x => new PixlabRectangle
            {
                X = x.Left,
                Y = x.Top,
                Height = x.Height,
                Width = x.Width
            });

            var drawRectanglesResponse = await _pixlabClient.DrawRectanglesAsync(originalPhotoUrl, faceRectangles);
            if (drawRectanglesResponse.IsFailure)
            {
                _logger.LogWarning(
                    "Couldn't draw rectangles for faces: {Status} {Error} {Image}",
                    drawRectanglesResponse.Status,
                    drawRectanglesResponse.Error,
                    originalPhotoUrl);
                return Result.Failure<string>(drawRectanglesResponse.Error);
            }

            return Result.Success(drawRectanglesResponse.Link);
        }

        private async Task<Result> SaveFaceDetectResponseAsync(string originalPhotoUrl, FaceDetectResponse response)
        {
            try
            {
                await _transactionManager.BeginTransactionAsync();

                var photoUpload = new PhotoUpload
                {
                    PhotoUrl = originalPhotoUrl,
                    FoundFacesCount = response.Faces.Length
                };

                await _photoUploadRepo.InsertAsync(photoUpload);


                foreach (var detectedFace in response.Faces)
                {
                    var photoUploadsFace = new PhotoUploadsFace
                    {
                        PhotoUploadId = photoUpload.Id,
                        PixlabId = detectedFace.FaceId,
                        PositionLeft = detectedFace.Left,
                        PositionTop = detectedFace.Top,
                        Width = detectedFace.Width,
                        Height = detectedFace.Height
                    };

                    await _photoUploadsFaceRepo.InsertAsync(photoUploadsFace);
                }

                await _transactionManager.CommitAsync();
                return Result.Success();
            }
            catch (Exception e)
            {
                await _transactionManager.RollbackAsync();
                _logger.LogError(e, "Couldn't save face detect response");
                return Result.Failure("Internal server error");
            }
        }
    }
}
