using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;
using HubPeople.Pixlab.Models;
using Microsoft.Extensions.Options;

namespace HubPeople.Pixlab
{
    internal class PixlabClient : IPixlabClient
    {
        private const string PixlabAPIBaseUrl = "https://api.pixlab.io/";

        private readonly HttpClient _httpClient;
        private readonly PixlabConfiguration _config;

        public PixlabClient(HttpClient httpClient, IOptions<PixlabConfiguration> options)
        {
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
            _httpClient.BaseAddress = new Uri(PixlabAPIBaseUrl);
            _config = options?.Value ?? throw new ArgumentException(
                $"Either {nameof(options)} or its value is null",
                nameof(options));
        }

        public async Task<FaceDetectResponse> DetectFacesAsync(string photoUrl)
        {
            var query = $"img={photoUrl}&key={_config.APIKey}";
            var detectFacesUri = new Uri($"facedetect?{query}", UriKind.Relative);
            var response = await _httpClient.GetAsync(detectFacesUri);
            var contentStream = await response.Content.ReadAsStreamAsync();
            var faceDetectResponse = await JsonSerializer.DeserializeAsync<FaceDetectResponse>(contentStream);
            return faceDetectResponse;
        }

        public async Task<DrawRectanglesResponse> DrawRectanglesAsync(string originalPhotoUrl, IEnumerable<PixlabRectangle> rectangles)
        {
            var drawRectanglesUri = new Uri("drawrectangles", UriKind.Relative);
            var request = new DrawRectanglesRequest
            {
                Img = originalPhotoUrl,
                Key = _config.APIKey,
                Cord = rectangles
            };

            var content = new StringContent(JsonSerializer.Serialize(request));
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = await _httpClient.PostAsync(drawRectanglesUri, content);
            var contentStream = await response.Content.ReadAsStreamAsync();
            var drawRectanglesResponse = await JsonSerializer.DeserializeAsync<DrawRectanglesResponse>(contentStream);
            return drawRectanglesResponse;
        }
    }
}
