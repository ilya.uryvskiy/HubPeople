using System.Collections.Generic;
using System.Threading.Tasks;
using HubPeople.Pixlab.Models;

namespace HubPeople.Pixlab
{
    public interface IPixlabClient
    {
        Task<FaceDetectResponse> DetectFacesAsync(string photoUrl);

        Task<DrawRectanglesResponse> DrawRectanglesAsync(string originalPhotoUrl, IEnumerable<PixlabRectangle> rectangles);
    }
}
