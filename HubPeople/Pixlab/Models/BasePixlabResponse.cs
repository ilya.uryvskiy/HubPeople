using System.Text.Json.Serialization;

namespace HubPeople.Pixlab.Models
{
    public class BasePixlabResponse
    {
        [JsonPropertyName("status")]
        public int Status { get; set; }

        [JsonPropertyName("error")]
        public string Error { get; set; }

        public bool IsFailure => Status != 200;
    }
}
