using System.Text.Json.Serialization;

namespace HubPeople.Pixlab.Models
{
    public class FaceDetectResponse : BasePixlabResponse
    {
        [JsonPropertyName("faces")]
        public DetectedFace[] Faces { get; set; }
    }
}
