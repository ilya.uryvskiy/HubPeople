using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace HubPeople.Pixlab.Models
{
    public class DrawRectanglesRequest
    {
        [JsonPropertyName("img")]
        public string Img { get; set; }

        [JsonPropertyName("key")]
        public string Key { get; set; }

        [JsonPropertyName("cord")]
        public IEnumerable<PixlabRectangle> Cord { get; set; }
    }
}
