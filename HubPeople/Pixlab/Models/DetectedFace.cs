using System.Text.Json.Serialization;

namespace HubPeople.Pixlab.Models
{
    public class DetectedFace
    {
        [JsonPropertyName("face_id")]
        public int FaceId { get; set; }

        [JsonPropertyName("left")]
        public int Left { get; set; }

        [JsonPropertyName("top")]
        public int Top { get; set; }

        [JsonPropertyName("width")]
        public int Width { get; set; }

        [JsonPropertyName("height")]
        public int Height { get; set; }
    }
}
