using System.Text.Json.Serialization;

namespace HubPeople.Pixlab.Models
{
    public class DrawRectanglesResponse : BasePixlabResponse
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("link")]
        public string Link { get; set; }
    }
}
