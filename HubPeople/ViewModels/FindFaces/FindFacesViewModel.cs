namespace HubPeople.ViewModels.FindFaces
{
    public class FindFacesViewModel
    {
        public bool IsSuccess { get; set; }

        public string ErrorMessage { get; set; }

        public string PhotoUrl { get; set; }
    }
}
